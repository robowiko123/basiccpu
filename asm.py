import sys
import string

class TokenizeError(Exception):
    pass

class CompileError(Exception):
    pass

register_names    = ["A", "B", "SP", "IP", "SEG"]
possible_pointers = ["[A]", "[B]", "[A++]", "[A--]", "[B++]", "[B--]", "[SP++]", "[--SP]"]
instr_argn_map = {
    "MOV": 2,
    "ADD": 2,
    "SUB": 2,
    "MUL": 2,
    "DIV": 2,
    "IFC": 0,
    "IFZ": 0,
    "IFG": 0,
    "IFL": 0,
    "IFN": 0,
    "CLF": 0,
    "CMP": 2,
}

arglen = {
    0: 0,
    1: 0,
    2: 0,
    3: 0,
    4: 1,
    5: 2,
    6: 0,
    7: 0,  # Invalid encoding
    8: 0,
    9: 0,
    10: 0,
    11: 0,
    12: 0,
    13: 0,
    14: 0,
    15: 0,
}

opcode_instr_map = {
    0: "MOV",
    1: "ADD",
    2: "SUB",
    3: "MUL",
    4: "DIV",
    5: "IFC",
    6: "IFZ",
    7: "IFG",
    8: "IFL",
    9: "IFN",
    10: "CLF",
    11: "CMP",
}

encoding_arg_map = {
    0:  "A",
    1:  "B",
    2:  "IP",
    3:  "SP",
    4:  "imm8",
    5:  "imm16",
    6:  "SEG",
    7:  "???",
    8:  "[--SP]",
    9:  "[SP++]",
    10: "[A++]",
    11: "[B++]",
    12: "[A--]",
    13: "[B--]",
    14: "[A]",
    15: "[B]",
}

instr_opcode_map = {
    "MOV": 0,
    "ADD": 1,
    "SUB": 2,
    "MUL": 3,
    "DIV": 4,
    "IFC": 5,
    "IFZ": 6,
    "IFG": 7,
    "IFL": 8,
    "IFN": 9,
    "CLF": 10,
    "CMP": 11,
}

reg_num_map = {
    "A": 0,
    "B": 1,
    "IP": 2,
    "SP": 3,
    "SEG": 6,
}

ptr_num_map = {
    "[--SP]": 8,
    "[SP++]": 9,
    "[A++]": 10,
    "[B++]": 11,
    "[A--]": 12,
    "[B--]": 13,
    "[A]": 14,
    "[B]": 15,
}

def tokenize(raw_data):
    by_newline = raw_data.split("\n")
    tokens = []
    current_line = 1
    if "\0" in raw_data:
        raise TokenizeError("NUL in input")
    for line in by_newline:
        line = line.split(";")[0]
        # print(line.replace("\t", " "))
        by_whitespace = line.strip().replace("\t", " ").split()
        # print(by_whitespace)
        if len(by_whitespace) == 0:   # Skip empty lines
            pass
        elif by_whitespace[0][-1] == ":":
            # Label
            if by_whitespace[0][0] not in string.ascii_letters:
                raise TokenizeError("at line %d: Label names must start with a letter" % (current_line))
            tokens.append((current_line, "labeldef", by_whitespace[0][:-1]))
        elif by_whitespace[0][0] == ".":
            # Metacommand
            tokens.append((current_line, "metacommand", by_whitespace[0][1:], by_whitespace[1:]))
        else:
            # Instruction
            tokens.append((current_line, "instruction", by_whitespace[0]))
            by_comma = " ".join(by_whitespace[1:]).split(",")
            if len(by_comma) > 1:
                # print(by_comma)
                by_comma[1] = by_comma[1].lstrip()
                by_comma[1] = ",".join(by_comma[1:])
                del by_comma[2:]
            # print(by_comma)
            for i in by_comma:
                if len(i) == 0:
                    continue
                if i in register_names:
                    tokens.append((current_line, "register", i))
                elif i in possible_pointers:
                    tokens.append((current_line, "pointer", i))
                elif (i not in possible_pointers) and i[0] == "[":
                    raise TokenizeError("at line %d: `%s` can't be used as a pointer" % (current_line, i[1:-1]))
                elif i[0] == "'" and (i[2] == "'" if len(i) > 2 else False):
                    tokens.append((current_line, "char", i[1]))
                elif all([i2 in string.digits for i2 in i]):
                    tokens.append((current_line, "imm", int(i)))
                elif i.startswith("0x") and all([i2 in string.hexdigits for i2 in i[2:]]):
                    tokens.append((current_line, "imm", int(i, 16)))
                elif i.startswith("0o") and all([i2 in string.octdigits for i2 in i[2:]]):
                    tokens.append((current_line, "imm", int(i, 8)))
                elif i.startswith("0b") and all([i2 in "01" for i2 in i[2:]]):
                    tokens.append((current_line, "imm", int(i, 2)))
                elif i[0] in string.ascii_letters:
                    tokens.append((current_line, "label", i))
                else:
                    raise TokenizeError("at line %d: Invalid syntax" % (current_line))
        current_line += 1
    return tokens

def compile(tokens):
    labels      = {}
    res         = [b"", ]
    section     = ""
    mode        = "normal"       # normal; argument
    argencoding = b"\0"
    args        = b""
    instr       = ""
    arg_counter = 0
    code_done   = False
    label_maybe = None
    for i in tokens:
        # print(i, res)
        if i[1] == "metacommand":
            if i[2] == "ORG":
                where = i[3][0]
                if all([i in string.digits for i in where]):
                    org = int(where, 10)
                elif where.startswith("0x") and all([i in string.hexdigits for i in where[2:]]):
                    org = int(where, 16)
                else:
                    raise CompileError("at line %d: Invalid syntax" % (i[0], ))
                res[-1] = b"\0" * org + res[-1]
            elif i[2] == "DATA":
                if code_done == False:
                    raise CompileError("at line %d: CODE section must precede DATA section" % (i[0], ))
                section = "data"
            elif i[2] == "CODE":
                section = "code"
                code_done = True
            else:
                raise CompileError("at line %d: Unknown metacommand" % (i[0], ))
        elif i[1] == "instruction":
            if section != "code":
                raise CompileError("at line %d: Instructions can only be used in CODE section" % (i[0], ))
            if i[2].upper() not in instr_opcode_map.keys():
                raise CompileError("at line %d: Unknown instruction `%s`" % (i[0], i[2]))
            if mode == "argument":
                mode = "normal"
                res[-1] += argencoding
                res[-1] += args
                if label_maybe:
                    res.append(label_maybe)
                    res.append(b"")
                label_maybe = None
                args = b""
                argsencoding = b"\0"
                arg_counter = 0

            mode = "argument"
            res[-1] += bytes([instr_opcode_map[i[2].upper()], ])
        elif i[1] == "labeldef":
            reslen = sum([2 if type(i) == str else len(i) for i in res]) + len(argencoding) + len(args)
            if section == "code":
                labels.update({i[2]: reslen})
            elif section == "data":
                labels.update({i[2]: reslen})
                res[-1] += b'\0' * 1024               # Maximum heap buffer size = 1024
        elif i[1] == "char":
            # 8-bit imm
            if arg_counter == 0:
                val = ord(i[2]) & 0xff
                argencoding = bytes([(argencoding[0] & 0xf0) | 4,])
                args += bytes([val, ])
            else:
                val = ord(i[2]) & 0xff
                argencoding = bytes([(argencoding[0] & 0x0f) | (4 << 4),])
                args += bytes([val, ])
            # print(args)
            arg_counter += 1
        elif i[1] == "register":
            if arg_counter == 0:
                argencoding = bytes([(argencoding[0] & 0xf0) | reg_num_map[i[2]],])
            else:
                argencoding = bytes([(argencoding[0] & 0x0f) | (reg_num_map[i[2]] << 4),])
            # print(hex(argencoding[0])[2:].zfill(2), i[0], args)
            arg_counter += 1
        elif i[1] == "label":
            if arg_counter == 0:
                argencoding = bytes([(argencoding[0] & 0xf0) | 5,])
            else:
                argencoding = bytes([(argencoding[0] & 0x0f) | (5 << 4),])

            label_maybe = i[2]

            arg_counter += 1
        elif i[1] == "imm":
            if arg_counter == 0:
                argencoding = bytes([(argencoding[0] & 0xf0) | 5,])
            else:
                argencoding = bytes([(argencoding[0] & 0x0f) | (5 << 4),])
            args += bytes([(i[2] & 0xff00) >> 8, i[2] & 0x00ff])
            arg_counter += 1
        elif i[1] == "pointer":
            if arg_counter == 0:
                argencoding = bytes([(argencoding[0] & 0xf0) | ptr_num_map[i[2]],])
            else:
                argencoding = bytes([(argencoding[0] & 0x0f) | (ptr_num_map[i[2]] << 4),])
            arg_counter += 1

    print(labels)
    actual_res = b"".join([i if type(i) == bytes else bytes([(labels[i] & 0xff00) >> 8, labels[i]]) for i in res])
    return actual_res

def disassemble(bin, addr=0x0040):
    def _R():
        nonlocal res, instr, encoding, arg1, arg2, chars_read
        res += opcode_instr_map[instr] + " "
        if encoding_arg_map[encoding & 0x0f] == "imm16":
            res += hex(arg1 + 1)
        elif encoding_arg_map[encoding & 0x0f] == "imm8":
            res += "'" + chr(arg1 + 1) + "'"
        else:
            res += encoding_arg_map[encoding & 0x0f]
        res += ", "
        if encoding_arg_map[(encoding & 0xf0) >> 4] == "imm16":
            res += hex(arg2 + 1)
        elif encoding_arg_map[(encoding & 0xf0) >> 4] == "imm8":
            res += "'" + chr(arg2 + 1) + "'"
        else:
             res += encoding_arg_map[(encoding & 0xf0) >> 4]
        res += "\n"
        instr = -1
        encoding = -1
        arg1 = -1
        arg2 = -1
        chars_read = 0

    res = ""
    instr = -1
    encoding = -1
    arg1 = -1
    arg2 = -1
    chars_read = 0
    for char in bin[addr:]:
        if instr == -1:
            instr = char
            if not opcode_instr_map.get(instr):
                res += "???\n"
                instr = -1
        elif encoding == -1:
            encoding = char
            if instr_argn_map[opcode_instr_map[instr]] == 0:
                res += opcode_instr_map[instr] + "\n"  # + encoding_arg_map[encoding & 0x0f] + ", " + encoding_arg_map[(encoding & 0xf0) >> 4]
                instr = -1
                encoding = -1
                chars_read = 0
        elif (encoding_arg_map[encoding & 0x0f] in ("imm8", "imm16")) and (arg1 == -1 or ((chars_read < 5) and (encoding_arg_map[encoding & 0x0f] == "imm16"))):
            arg1 += (char if chars_read == 3 else char * 256)
        else:
            arg2 += (char if chars_read != 6 else char * 256)
            # print("here2")
            if (encoding_arg_map[(encoding & 0xf0) >> 4] == "imm16") and (chars_read < (3 + arglen[encoding & 0x0f])):
                pass  #print("here")
            else:
                _R()
        # print(instr, encoding, arg1, arg2, chars_read)
        chars_read += 1
    _R()
    return res

with open(sys.argv[1] if len(sys.argv) > 1 else input("file: ")) as f:
    raw_data = f.read()

tokens = tokenize(raw_data)
bin = compile(tokens)

if len(sys.argv) < 2:
    for i in range(len(bin)):
        if i % 8 == 0:
            print("0x" + hex(i)[2:].zfill(4), "|", end=" ")
        print(hex(bin[i])[2:].zfill(2), end=" ")
        if i % 8 == 7:
            print()
else:
    with open(sys.argv[2], "wb") as f:
        f.write(bin)
