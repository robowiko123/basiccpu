; Possible arguments:            Examples:
;   number                       1
;   pointer                      [A]
;   single character             'h'
;   registers                    A
;   labels                       halt

; Pointer syntax:
;   [thing]
; Allowed pointers:
;   [A]
;   [B]
;   [A++]
;   [B++]
;   [A--]
;   [B--]
;   [SP++]
;   [--SP]

; Instruction syntax:
;   [instr] [arg1], [arg2]
;   OR
;   [labelname]:

; Anything after a ';' symbol should be ignored.
; Any whitespace before or after an instruction should be ignored.
; Any whitespace characters separating instr from arg1 should be consolidated into 1 space
; Any whitespace between ',' and [arg2] should be removed.
; Test program:

.CODE
.ORG 0x0040  ; Removed for testing

;; liczba + '0'
;; znak - '0'

first:
        mov A, 0x1000
        mov B, 0x2000
        mov [A], 'P'
        mov [A], 'o'
        mov [A], 'd'
        mov [A], 'a'
        mov [A], 'j'
        mov [A], ' '
        mov [A], 'p'
        mov [A], 'i'
        mov [A], 'e'
        mov [A], 'r'
        mov [A], 'w'
        mov [A], 's'
        mov [A], 'z'
        mov [A], 'a'
        mov [A], ' '
        mov [A], 'l'
        mov [A], 'i'
        mov [A], 'c'
        mov [A], 'z'
        mov [A], 'b'
        mov [A], 'e'
        mov [A], ':'
        mov [A], ' '
        mov [B], [A]

second:
        mov B, 0x2100
        mov [A], 'P'
        mov [A], 'o'
        mov [A], 'd'
        mov [A], 'a'
        mov [A], 'j'
        mov [A], ' '
        mov [A], 'd'
        mov [A], 'r'
        mov [A], 'u'
        mov [A], 'g'
        mov [A], 'a'
        mov [A], ' '
        mov [A], 'l'
        mov [A], 'i'
        mov [A], 'c'
        mov [A], 'z'
        mov [A], 'b'
        mov [A], 'e'
        mov [A], ':'
        mov [A], ' '
        mov [B], [A]

result:
        mov [A], 'W'
        mov [A], 'y'
        mov [A], 'n'
        mov [A], 'i'
        mov [A], 'k'
        mov [A], ':'
        mov [A], ' '
        mov A, 0x2000
        add [B], [A]
        sub [B], '0'
        mov A, 0x1000
        mov [A], B

halt:
        ; mov IP, halt ; Loop forever so that we don't execute garbage

        ; Some recognisable values
        mov A, A
        mov B, A
        mov IP, A
        mov SP, A
        ; mov 0x0, A  ; Invalid instruction encoding
        mov SEG, A

.DATA
