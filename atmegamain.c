#define F_CPU 16000000L

#include <util/delay.h>
#include <avr/io.h>

// PB6; DDRB6 - pin 13
// PB7; DDRB7 - pin 12

int main() {
	DDRB |= 0b11111111;
	while (1) {
		PORTB = 0xff;
		_delay_ms(1000);
		PORTB = 0x00;
		_delay_ms(1000);
	}
}