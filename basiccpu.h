#ifndef BASICCPU_H
#define BASICCPU_H

#include <inttypes.h>
#include <unistd.h>

int test_main(int argc, char **argv);
void set_thing(uint8_t);
void _set_thing(uint8_t, uint16_t);
uint16_t get_thing(uint8_t);
void aluflagset(uint16_t, uint16_t, long);
void tick();
int GETMEM(long);
void SETMEM(long, int);

#endif // BASICCPU_H