# BasicCPU

TODO: Fix the assembler

This repository contains:

* Documentation about the CPU
* Code emulating the CPU
* A very broken assembler (syntax somewhat similar to NASM)

Note: The example assembly program uses address 0x1000 as the way to send and receive characters from the terminal. This is not standard - I haven't come up with a practical address yet and it will probably be changed.

## Assembler documentation

### Instructions

There are 13 instructions.

* MOV arg1, arg2 - Moves data from arg2 to arg1
* ADD arg1, arg2 - Adds arg1 and arg2. Stores the result in arg1.
* SUB arg1, arg2 - Subtracts arg2 from arg1. Stores the result in arg1.
* MUL arg1, arg2 - Multiplies arg1 and arg2. Stores the result in arg1.
* DIV arg1, arg2 - Divides arg1 by arg2. Stores the result in arg1.
* IFC            - Skips instruction if Carry flag not set
* IFZ            - Skips instruction if Zero flag not set
* IFG            - Skips instruction if Greater than flag not set
* IFL            - Skips instruction if Less than flag not set
* IFN            - Skips instruction if Not Zero flag not set

You will probably use the MOV instruction for most operations.

### Registers

There are 6 registers.

* A - General purpose register.
* B - General purpose register.
* IP - Instruction pointer.
* SP - Stack pointer. Can be used as a general purpose register
* SEG - Segment register. Determines which segment the read operations and write operations take place in. A segment is a 64K part of memory. There are 2^16 segments.
* F - Flags register. Cannot be used directly.

TODO: Add support for the SEG register into the emulator.

### Pointers

To access memory based on a register value, use: [register].
You can only access memory with the A, B, and SP registers.

Possible pointer syntax:
* [A]
* [B]
* [A++]
* [B++]
* [A--]
* [B--]
* [SP++]
* [--SP]

NOTE: You cannot access memory using SP without changing it.

TODO: finish docs

## Metacommands

There are 4 metacommands:
* .ORG [integer] - Defines entry point location
* .CODE - Defines start of code section
* .DATA - Defines start of data section
* .const [integer/char #1] [integer/char #2] ... [integer/char #n] - Put data into output

## ABI

TODO

## CPU Documentation

Note: Copied from a file that I wrote in 15 minutes to help me write the emulator.
TODO: Clean this section up[!]

Entry point 0x0040

Big endian architecture

A  16 bit
B  16 bit
IP 16 bit
SP 16 bit
F  16 bit
SEG 16 bit

[F]lags register:
  0    0    0    0    0    0    0    0    0    0    0    0    0    0    0    0
                                                        NZ    G    L    Z    C
C - carry
Z - zero
L - less
G - greater
NZ - not zero

opcode argtype ...
00     00      ...

RST 00h (addr 00h) - Invalid instruction (or instruction encoding)
RST 01h (addr 08h) - Divide by 0
RST 02h (addr 10h) - 
RST 03h (addr 18h) - 
RST 04h (addr 20h) - 
RST 05h (addr 28h) - 
RST 06h (addr 30h) - 
RST 07h (addr 38h) - NMI (VBLANK) vector
RST 08h (addr 40h) - RESET vector

argtype defines the meaning / value of both arguments. The high nibble defines the 2nd argument and the low defines the 1st.

1st <- 2nd

Value meaning:
  0 - A
  1 - B
  2 - IP
  3 - SP
  4 - imm8
  5 - imm16
  6 - SEG
  7 - 
  8 - [--SP]
  9 - [SP++]
  A - [A++]
  B - [B++]
  C - [A--]
  D - [B--]
  E - [A]
  F - [B]

Opcodes: (arg1 is always the written arg and arg2 is the one that gets read)

0x00 - MOV (defines multiple instructions that move something to something else such as push, pop, jmp, etc.)
0x01 - ADD
0x02 - SUB
0x03 - MUL
0x04 - DIV
0x05 - IFC skip 1 instruction if C not set
0x06 - IFZ skip 1 instruction if Z not set
0x07 - IFG skip 1 instruction if G not set
0x08 - IFL skip 1 instruction if L not set
0x09 - IFN skip 1 instruction if NZ not set
0x0A - CLF clear flags
0x0B - CMP subtract without saving value (only sets flags)

Example:

MOV A, 0x0050    ; Single char buf  (writing to it prints char on screen, reading from it returns 0x00)
MOV [A], 'H'
MOV [A], 'e'
MOV [A], 'l'
MOV [A], 'l'
MOV [A], 'o'
MOV [A], '!'

00 50 00 50
00 4e 48
00 4e 65
00 4e 6c
00 4e 6c
00 4e 6f
00 4e 21