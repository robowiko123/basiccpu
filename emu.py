# Lazily converted from C

import sys

SCREEN_LOCATION = 0x1000

MEM_SIZE = 65536

INSTR_MOV = 0x00
INSTR_ADD = 0x01
INSTR_SUB = 0x02
INSTR_MUL = 0x03
INSTR_DIV = 0x04
INSTR_IFC = 0x05
INSTR_IFZ = 0x06
INSTR_IFG = 0x07
INSTR_IFL = 0x08
INSTR_IFN = 0x09
INSTR_CLF = 0x0A
INSTR_CMP = 0x0B

class _CPURegisters():
	def __init__(self):
		self.A = 0
		self.B = 0
		self.SP = 0
		self.IP = 0
		self.SEG = 0
		self.F_bin = _flags()

	def f_set(self, x):
		self.F_bin.set_dec(x)

	def F_get(self):
		return self.F_bin.to_dec()
	
	F = property(fset=f_set, fget=F_get)

class _flags():
	def __init__(self):
		self.L = 0
		self.G = 0
		self.Z = 0
		self.NZ = 0
		self.C = 0
	def to_dec(self):
		return NZ * 16 + G * 8 + L * 4 + Z * 2 + C * 1
	def set_dec(self, x):
		if x == 0:
			self.L = 0
			self.G = 0
			self.Z = 0
			self.NZ = 0
			self.C = 0
		else:
			raise Exception("arg to set_dec must be 0")

def getch():
    if not hasattr(sys.stdout, "fileno"):
        # Fake shell
        return sys.stdout.read(1)  # Waits for <Enter>
    try:
        import msvcrt
        return msvcrt.getch()
    except ImportError as e:
        return sys.stdout.read(1)  # Waits for <Enter>

def SETMEM(addr, x):
	global mem
	mem[addr] = x

def GETMEM(addr):
	global mem
	if addr == SCREEN_LOCATION:
		return getch()[0]
	else:
		return mem[addr]

def set_thing(what):
	thing = get_thing((what & 0xf0) >> 4)
	_set_thing(what, thing)

def _set_thing(what, thing):
	global CPURegisters
	
	# Assuming that IP points to the byte after the encoding byte
	tmp = (what & 0x0f)
	if tmp == 0:
		# A <- thing
		CPURegisters.A = thing
	elif tmp == 1:
		# B <- thing
		CPURegisters.B = thing
	elif tmp == 2:
		# IP <- thing
		CPURegisters.IP = thing
	elif tmp == 3:
		# SP <- thing
		CPURegisters.SP = thing
	elif tmp == 4:
		# imm8 <- thing
		CPURegisters.IP = 0
	elif tmp == 5:
		# imm16 <- thing
		CPURegisters.IP = 0
	elif tmp == 6:
		# SEG <- thing
		CPURegisters.SEG = thing
	elif tmp == 7:
		# ??? <- thing
		CPURegisters.IP = 0
	elif tmp == 8:
		# [--SP] <- thing
		CPURegisters.SP -= 1
		SETMEM(CPURegisters.SP, thing & 0x00ff)
	elif tmp == 9:
		# [SP++] <- thing
		SETMEM(CPURegisters.SP, thing & 0x00ff)
		CPURegisters.SP += 1
	elif tmp == 10:
		# [A++] <- thing
		SETMEM(CPURegisters.A, thing & 0x00ff)
		CPURegisters.A += 1
	elif tmp == 11:
		# [B++] <- thing
		SETMEM(CPURegisters.B, thing & 0x00ff)
		CPURegisters.B += 1
	elif tmp == 12:
		# [A--] <- thing
		SETMEM(CPURegisters.A, thing & 0x00ff)
		CPURegisters.A -= 1
	elif tmp == 13:
		# [B--] <- thing
		SETMEM(CPURegisters.B, thing & 0x00ff)
		CPURegisters.B -= 1
	elif tmp == 14:
		# [A] <- thing
		SETMEM(CPURegisters.A, thing & 0x00ff)
	elif tmp == 15:
		# [B] <- thing
		SETMEM(CPURegisters.B, thing & 0x00ff)

def get_thing(what):
	global CPURegisters
	tmp = what & 0x0f
	if tmp == 0:
		# A
		return CPURegisters.A
	elif tmp == 1:
		# B
		return CPURegisters.B
	elif tmp == 2:
		# IP
		return CPURegisters.IP
	elif tmp == 3:
		# SP
		return CPURegisters.SP
	elif tmp == 4:
		# imm8
		tmp2 = CPURegisters.IP
		CPURegisters.IP += 1
		return GETMEM(tmp2)
	elif tmp == 5:
		# imm16
		x = GETMEM(CPURegisters.IP)
		CPURegisters.IP += 1
		y = GETMEM(CPURegisters.IP)
		CPURegisters.IP += 1
		return x * 256 + y
	elif tmp == 6:
		# SEG
		return CPURegisters.SEG
	elif tmp == 7:
		# ???
		CPURegisters.IP = 0
		return -1
	elif tmp == 8:
		# [--SP]
		CPURegisters.SP -= 1
		return GETMEM(CPURegisters.SP)
	elif tmp == 9:
		x = GETMEM(CPURegisters.SP)
		CPURegisters.SP += 1
		return x
	elif tmp == 10:
		# [A++]
		x = GETMEM(CPURegisters.A)
		CPURegisters.A += 1
		return x
	elif tmp == 11:
		# [B++]
		x = GETMEM(CPURegisters.B)
		CPURegisters.B += 1
		return x
	elif tmp == 12:
		# [A--]
		x = GETMEM(CPURegisters.A)
		CPURegisters.A -= 1
		return x
	elif tmp == 13:
		# [B--]
		x = GETMEM(CPURegisters.B)
		CPURegisters.B -= 1
		return x
	elif tmp == 14:
		# [A]
		x = GETMEM(CPURegisters.A)
		return x
	elif tmp == 15:
		# [B]
		x = GETMEM(CPURegisters.B)
		return x
	return -1

def aluflagset(arg1, arg2, opres):
	global CPURegisters
	if arg1 < arg2:
		CPURegisters.F_bin.L = 1
	else:
		CPURegisters.F_bin.L = 0
	if arg1 > arg2:
		CPURegisters.F_bin.G = 1
	else:
		CPURegisters.F_bin.G = 0
	if arg1 == arg2:
		CPURegisters.F_bin.Z = 1
		CPURegisters.F_bin.NZ = 0
	else:
		CPURegisters.F_bin.Z = 0
		CPURegisters.F_bin.NZ = 1
	if opres < 0:
		CPURegisters.F_bin.C = 1
	elif opres > 65535:
		CPURegisters.F_bin.C = 1
	else:
		CPURegisters.F_bin.C = 0

skip_instr = 0
def tick(invalid_instr=None):
	global CPURegisters, skip_instr

	# Opcode
	opcode = GETMEM(CPURegisters.IP)
	CPURegisters.IP += 1
	# Argument types
	what = GETMEM(CPURegisters.IP)
	CPURegisters.IP += 1
	
	if skip_instr:
		skip_instr = False
		if (what & 0x0f) > 7:
			CPURegisters.IP += 1
		if ((what & 0xf0) >> 4) > 7:
			CPURegisters.IP += 1
		return
	if opcode == INSTR_MOV:
		set_thing(what)
	elif opcode == INSTR_ADD:
		arg1 = get_thing((what % 0xf0) >> 4)
		arg2 = get_thing(what)
		aluflagset(arg1, arg2, arg1 + arg2)
		_set_thing(what, arg1 + arg2)
	elif opcode == INSTR_SUB:
		arg1 = get_thing((what % 0xf0) >> 4)
		arg2 = get_thing(what)
		aluflagset(arg1, arg2, arg1 - arg2)
		_set_thing(what, arg1 - arg2)
	elif opcode == INSTR_MUL:
		arg1 = get_thing((what % 0xf0) >> 4)
		arg2 = get_thing(what)
		aluflagset(arg1, arg2, arg1 * arg2)
		_set_thing(what, arg1 * arg2)
	elif opcode == INSTR_DIV:
		arg1 = get_thing((what % 0xf0) >> 4)
		arg2 = get_thing(what)
		if arg2 == 0:
			# Division by 0
			CPURegisters.IP = 0x10
		else:
			aluflagset(arg1, arg2, arg1 / arg2)
			_set_thing(what, arg1 / arg2)
	elif opcode == INSTR_IFC:
		if not CPURegisters.F_bin.C:
			skip_instr = True
	elif opcode == INSTR_IFZ:
		if not CPURegisters.F_bin.Z:
			skip_instr = True
	elif opcode == INSTR_IFG:
		if not CPURegisters.F_bin.G:
			skip_instr = True
	elif opcode == INSTR_IFL:
		if not CPURegisters.F_bin.L:
			skip_instr = True
	elif opcode == INSTR_IFN:
		if not CPURegisters.F_bin.NZ:
			skip_instr = True
	elif opcode == INSTR_CLF:
		CPURegisters.F = 0
	elif opcode == INSTR_CMP:
		# Basically SUB but w/o setting the values
		arg1 = get_thing((what % 0xf0) >> 4)
		arg2 = get_thing(what)
		aluflagset(arg1, arg2, arg1 - arg2)
	else:
		if invalid_instr:
			invalid_instr()
		else:
			print("Invalid instruction [%x] at %x\n" % (opcode, CPURegisters.IP - 2))
			raise Exception("CPU halted.")

def test_main(argc, argv):
	global CPURegisters, mem
	debug = 0

	CPURegisters.SEG = 0x0000
	CPURegisters.IP  = 0x0040
	CPURegisters.SP  = 0x0000
	CPURegisters.A   = 0x0000
	CPURegisters.B   = 0x0000
	CPURegisters.F   = 0x0000
	
	if argc > 1:
		pFile = open(argv[1], "rb")
		tmp = pFile.read()
		i = 0
		while i < len(tmp):
			mem[i] = tmp[i]
			i += 1
		pFile.close()
	else:
		print("No program specified.")
		sys.exit(1)

	if argc > 2:
		if argv[2] == "debug":
			debug = 1

	while 1:
		tick()
		if debug:
			print("AFTER A: %x | B: %x | SP: %x | IP: %x" % (
				CPURegisters.A,
				CPURegisters.B,
				CPURegisters.SP,
				CPURegisters.IP,
			))
		if mem[SCREEN_LOCATION]:
			print(chr(mem[SCREEN_LOCATION]), end="", flush=True)
		mem[SCREEN_LOCATION] = 0
	return 0

mem = bytearray(MEM_SIZE)
CPURegisters = _CPURegisters()
test_main(len(sys.argv), sys.argv)
