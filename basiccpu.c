// I wrote most of this in Notepad.

#include "basiccpu.h"
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>

#define INSTR_MOV 0x00
#define INSTR_ADD 0x01
#define INSTR_SUB 0x02
#define INSTR_MUL 0x03
#define INSTR_DIV 0x04
#define INSTR_IFC 0x05
#define INSTR_IFZ 0x06
#define INSTR_IFG 0x07
#define INSTR_IFL 0x08
#define INSTR_IFN 0x09
#define INSTR_CLF 0x0A
#define INSTR_CMP 0x0B

#define SCREEN_LOCATION 0x1000

#ifndef false
  #define false 0
  #define true  1
#endif // false

#define MEM_SIZE 65536     // 1 K

struct _CPURegisters {
  uint16_t A;
  uint16_t B;
  uint16_t SP;
  uint16_t IP;
  uint16_t SEG;
  union {
    uint16_t flags;
    struct {
      uint16_t : 11;
      uint16_t NZ : 1;
      uint16_t  G : 1;
      uint16_t  L : 1;
      uint16_t  Z : 1;
      uint16_t  C : 1;
    } flags_bin;
  } F;
} CPURegisters;

uint8_t mem[MEM_SIZE];
int skip_instr;

void SETMEM(long addr, int x) {
        mem[addr] = x;
}

int GETMEM(long addr) {
        if (addr == SCREEN_LOCATION) {
                return getchar();
        } else {
                return mem[addr];
        }
}

void set_thing(uint8_t what) {
  uint16_t thing = get_thing((what & 0xf0) >> 4);
  _set_thing(what, thing);
}

/////////////////////////////////////////////////////////////////////////////////////

#ifdef __linux__
        #include <termios.h>
        struct termios orig_termios;
#else
        #include <windows.h>
#endif

void disableRawMode();
void enableRawMode();

void disableRawMode() {
        #ifdef __linux__
                tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios);
        #else
                HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
                DWORD mode = 0;
                GetConsoleMode(hStdin, &mode);
                SetConsoleMode(hStdin, mode | ENABLE_ECHO_INPUT | ENABLE_LINE_INPUT);
        #endif
}

void enableRawMode() {
        #ifdef __linux__
                tcgetattr(STDIN_FILENO, &orig_termios);
                atexit(disableRawMode);
                struct termios raw = orig_termios;
                raw.c_lflag &= ~(ECHO);
                raw.c_lflag &= ~(ICANON);
                tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
        #else
                HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
                DWORD mode = 0;
                GetConsoleMode(hStdin, &mode);
                SetConsoleMode(hStdin, mode & (~ENABLE_ECHO_INPUT) & (~ENABLE_LINE_INPUT));
        #endif
}

/////////////////////////////////////////////////////////////////////////////////////

void _set_thing(uint8_t what, uint16_t thing) {
  // Assuming that IP points to the byte after the encoding byte
  switch (what & 0x0f) {
  case 0:       // A <- thing
//    printf("A <- thing\n");
    CPURegisters.A = thing;
    break;
  case 1:       // B <- thing
    CPURegisters.B = thing;
    break;
  case 2:       // IP <- thing
    CPURegisters.IP = thing;
    break;
  case 3:       // SP <- thing
    CPURegisters.SP = thing;
    break;
  case 4:       // imm8 <- thing  [INVALID ENCODING]
    CPURegisters.IP = 00;  // RST 00h
    break;
  case 5:       // imm16 <- thing  [INVALID ENCODING]
    CPURegisters.IP = 00;  // RST 00h
    break;
  case 6:       // SEG <- thing
    CPURegisters.SEG = thing;
    break;
  case 7:       // ??? <- thing  [INVALID ENCODING]
    CPURegisters.IP = 00;  // RST 00h
    break;
  case 8:       // [--SP] <- thing
    CPURegisters.SP--;
    SETMEM(CPURegisters.SP, thing & 0x00ff);
    break;
  case 9:       // [SP++] <- thing
    SETMEM(CPURegisters.SP, thing & 0x00ff);
    CPURegisters.SP++;
    break;
  case 10:      // [A++] <- thing
    SETMEM(CPURegisters.A, thing & 0x00ff);
    CPURegisters.A++;
    break;
  case 11:      // [B++] <- thing
    SETMEM(CPURegisters.B, thing & 0x00ff);
    CPURegisters.B++;
    break;
  case 12:      // [A--] <- thing
    SETMEM(CPURegisters.A, thing & 0x00ff);
    CPURegisters.A--;
    break;
  case 13:      // [B--] <- thing
    SETMEM(CPURegisters.B, thing & 0x00ff);
    CPURegisters.B++;
    break;
  case 14:      // [A] <- thing
    SETMEM(CPURegisters.A, thing & 0x00ff);
    break;
  case 15:      // [B] <- thing
    SETMEM(CPURegisters.B, thing & 0x00ff);
    break;
  }
}

uint16_t get_thing(uint8_t what) {
  // Assuming that IP points to the byte after the encoding byte
  char x;
  switch (what & 0x0f) {
  case 0:       // A
    return CPURegisters.A;
  case 1:       // B
    return CPURegisters.B;
  case 2:       // IP
    return CPURegisters.IP;
  case 3:       // SP
    return CPURegisters.SP;
  case 4:       // imm8
    return GETMEM(CPURegisters.IP++);
  case 5:       // imm16
    x = GETMEM(CPURegisters.IP++);
    return x * 256 + mem[CPURegisters.IP++];
  case 6:       // SEG
    return CPURegisters.SEG;
  case 7:       // ???  [INVALID ENCODING]
    CPURegisters.IP = 00; return -1;  // RST 00h
  case 8:       // [--SP]
    return GETMEM(--CPURegisters.SP);
  case 9:       // [SP++]
    return GETMEM(CPURegisters.SP++);
  case 10:      // [A++]
    return GETMEM(CPURegisters.A++);
  case 11:      // [B++]
    return GETMEM(CPURegisters.B++);
  case 12:      // [A--] <- thing
    return GETMEM(CPURegisters.A--);
  case 13:      // [B--]
    return GETMEM(CPURegisters.B--);
  case 14:      // [A]
    return GETMEM(CPURegisters.A);
  case 15:      // [B]
    return GETMEM(CPURegisters.B);
  }
  return -1;
}

void aluflagset(uint16_t arg1, uint16_t arg2, long opres) {
  if (arg1 < arg2)  CPURegisters.F.flags_bin.L  = 1; else CPURegisters.F.flags_bin.L  = 0;
  if (arg1 > arg2)  CPURegisters.F.flags_bin.G  = 1; else CPURegisters.F.flags_bin.G  = 0;
  if (arg1 == arg2) CPURegisters.F.flags_bin.Z  = 1; else CPURegisters.F.flags_bin.Z  = 0;
  if (arg1 != arg2) CPURegisters.F.flags_bin.NZ = 1; else CPURegisters.F.flags_bin.NZ = 0;
  if (opres < 0) {
    CPURegisters.F.flags_bin.C = 1;
  } else if (opres > 65535) {
    CPURegisters.F.flags_bin.C = 1;
  } else {
    CPURegisters.F.flags_bin.C = 0;
  }
}

void tick() {
  uint8_t opcode   = mem[CPURegisters.IP++];
  uint8_t what     = mem[CPURegisters.IP++];
  uint16_t arg1;
  uint16_t arg2;
  if (skip_instr) {
    if ((what & 0x0f) > 7) CPURegisters.IP++;
    if ((what & 0xf0 >> 4) > 7) CPURegisters.IP++;
    skip_instr = false;
    return;
  }
  switch (opcode) {
  case INSTR_MOV:
    set_thing(what);
    break;
  case INSTR_ADD:
    arg1 = get_thing(what & 0xf0 >> 4);
    arg2 = get_thing(what);
    aluflagset(arg1, arg2, ((long) arg1) + ((long) arg2));
    _set_thing(what, arg1 + arg2);
    break;
  case INSTR_SUB:
    arg1 = get_thing(what & 0xf0 >> 4);
    arg2 = get_thing(what);

    aluflagset(arg1, arg2, ((long) arg1) - ((long) arg2));

    _set_thing(what, arg1 - arg2);
    break;
  case INSTR_MUL:
    arg1 = get_thing(what & 0xf0 >> 4);
    arg2 = get_thing(what);
    aluflagset(arg1, arg2, ((long) arg1) * ((long) arg2));
    _set_thing(what, arg1 * arg2);
    break;
  case INSTR_DIV:
    arg1 = get_thing(what & 0xf0 >> 4);
    arg2 = get_thing(what);
    if (arg2 == 0) {
      // Whoops, no division by 0 allowed
      CPURegisters.IP = 0x10;
    } else {
      aluflagset(arg1, arg2, ((long) arg1) / ((long) arg2));
      _set_thing(what, arg1 / arg2);
    }
    break;
  case INSTR_IFC:
    if (!(CPURegisters.F.flags_bin.C)) skip_instr = true;
    break;
  case INSTR_IFZ:
    if (!(CPURegisters.F.flags_bin.Z)) skip_instr = true;
    break;
  case INSTR_IFG:
    if (!(CPURegisters.F.flags_bin.G)) skip_instr = true;
    break;
  case INSTR_IFL:
    if (!(CPURegisters.F.flags_bin.L)) skip_instr = true;
    break;
  case INSTR_IFN:
    if (!(CPURegisters.F.flags_bin.NZ)) skip_instr = true;
    break;
  case INSTR_CLF:
    CPURegisters.F.flags = 0;
    break;
  case INSTR_CMP:
    arg1 = get_thing(what & 0xf0 >> 4);
    arg2 = get_thing(what);
    aluflagset(arg1, arg2, (long) arg1 - (long) arg2);
    break;
  default:
    printf("Invalid instruction [%d] at %d\n", opcode, CPURegisters.IP);
        exit(1);
  }
}

int test_main(int argc, char **argv) {
  // Reset vector is at 0x00000040
  FILE * pFile;

  CPURegisters.SEG = 0x0000;
  CPURegisters.IP  = 0x0040;

  CPURegisters.SP = 0x0000;
  CPURegisters.A  = 0x0000;
  CPURegisters.B  = 0x0000;

  CPURegisters.F.flags = 0x0000;

  if (argc == 2) {
    pFile = fopen(argv[1], "r");
        int c;
        int i = 0;
    while((c = fgetc(pFile)) != EOF) {
                mem[i++] = c;
        }
        fclose(pFile);
        printf("Loaded program.\n");
  } else {
    printf("No program specified. Executing test program.\n");
  // Test code
        mem[0x40] = 0x00;
        mem[0x41] = 0x50;
        mem[0x42] = 0x00;
        mem[0x43] = 0x50;

        mem[0x44] = 0x00;
        mem[0x45] = 0x4e;
        mem[0x46] = 'H';

        mem[0x47] = 0x00;
        mem[0x48] = 0x4e;
        mem[0x49] = 'e';

        mem[0x4a] = 0x00;
        mem[0x4b] = 0x4e;
        mem[0x4c] = 'l';

        mem[0x4d] = 0x00;
        mem[0x4e] = 0x4e;
        mem[0x4f] = 'l';

        mem[0x50] = 0x00;
        mem[0x51] = 0x4e;
        mem[0x52] = 'o';

        mem[0x53] = 0x00;
        mem[0x54] = 0x4e;
        mem[0x55] = '!';

        mem[0x56] = 0x00;
        mem[0x57] = 0x52;
        mem[0x58] = 0x00;
        mem[0x59] = 0x56;
  }

  enableRawMode();

  while (1) {
    // fprintf(stderr, "BEFORE A: %d | B: %d | SP: %d | IP: %d\n", CPURegisters.A, CPURegisters.B, CPURegisters.SP, CPURegisters.IP);
    tick();
    // fprintf(stderr, "AFTER  A: %d | B: %d | SP: %d | IP: %d\n", CPURegisters.A, CPURegisters.B, CPURegisters.SP, CPURegisters.IP);
    if (mem[SCREEN_LOCATION]) printf("%c", mem[SCREEN_LOCATION]);
    mem[SCREEN_LOCATION] = 0;
  }
  return 0;
}
